"use strict";
let tasks = [];

document.addEventListener("DOMContentLoaded", setup);
//comment 
function setup() {
    let form = document.querySelector("form");
    form.addEventListener("submit", submit);

    loadToDo();

    let container = document.querySelector("#container");    
    container.addEventListener("change", checkedAction);
}

function saveTasks(){
    let json = JSON.stringify(tasks);
    localStorage.setItem("tasks",json);
}

function checkedAction(evt) {
    const taskToRemove = evt.target.closest("article");
    const index = parseInt(taskToRemove.id);
    taskToRemove.remove();

    for(let i = index; i < tasks.length-1;i++ ) {
        let taskToUpdate = document.getElementById(i + 1);
        taskToUpdate.id = i;
        tasks[i] = tasks[i+1];
    }
    tasks.pop();

    saveTasks();
}

function loadToDo() {
    let json = localStorage.getItem("tasks");
    if (json){
        tasks=JSON.parse(json);
    }
    for (let item of tasks) {
        if(item){
            addTask(item);
        }
    }
}

function submit(evt){
    evt.preventDefault();

    const title = document.querySelector("#task").value;
    const descrip = document.querySelector("#description").value;
    const importanceLVL = document.querySelector("#importance").value;
    const category = document.querySelector("input[name='category']:checked").id;

    let newTaskObj = {
        title: title,
        descrip: descrip,
        importance: importanceLVL,
        category: category,
    };

    tasks.push(newTaskObj);
    addTask(newTaskObj);

    saveTasks();

    document.querySelector("form").reset(); 
}

function addTask(taskObj){
    const sect = document.querySelector("#container");
    const template = document.querySelector("#template");
    let newTask = template.cloneNode(true);

    newTask.querySelector(".title").textContent=taskObj.title;
    newTask.querySelector(".taskDescrip").textContent = taskObj.descrip;
    newTask.querySelector(".taskDiv").id = taskObj.category + "Color";
    newTask.querySelector(".stars").innerHTML = importanceStars(taskObj.importance);
    newTask.id = tasks.indexOf(taskObj);

    sect.appendChild(newTask);
}

function importanceStars(lvl){
    let text;
    switch(lvl){
        case "0":
            text="&#x2606;  &#x2606; &#x2606;";
        break;
        case "1":
            text="&#x2605;  &#x2606; &#x2606;";
        break;
        case "2":
            text="&#x2605;  &#x2605; &#x2606;";
        break;
        case "3":
            text="&#x2605;  &#x2605; &#x2605;";
        break;
    }
    return text;
}
